import {ADD_TODO,DELETE_TODO,TOGGLE_TODO} from '../constants';
import { filter, map } from 'lodash';
 
const todos = (state = [],action) => {
	let todos = null;
	switch(action.type){
		case ADD_TODO:
			
			todos =[
				...state,
				{
					todoElem:action.todoElem,
					todoId:action.todoId,
					toggled:action.toggled,	
				}
			];
			return todos;
			
		case DELETE_TODO:
			todos= state.filter(todos => todos.todoId!==action.todoId);
			return todos;
			
		case TOGGLE_TODO:
					todos = state.map(todo => {
						if(todo.todoId!==action.todoId){
							return todo;
						}
						return  {
							todoElem:todo.todoElem,
							toggled:!todo.toggled,
							todoId:todo.todoId,
						};
					});
					return todos;
		default:
			return state;
	}
}

export default todos;
