import {ADD_TODO,DELETE_TODO,TOGGLE_TODO } from '../constants';

export const addTodo = (todoText) => {
	const action = {
		type: ADD_TODO,						
		todoElem:todoText.todoElem,
		todoId:todoText.todoId,
		toggled:todoText.toggled,
	}
	return action;
}

export const deleteTodo = (todoId) => {
	const action = {
		type:DELETE_TODO,
		todoId
	}
	return action;
}

export const toggleTodo = (todoId) =>{
	const action ={
		type:TOGGLE_TODO,
		todoId,
	}
	return action;
} 
