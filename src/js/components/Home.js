import React from 'react';
import { connect } from 'react-redux'; 
import { addTodo ,deleteTodo,toggleTodo } from '../actions';
import { filter, map } from 'lodash';
const uuidv1 = require('uuid/v1');

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		
		};
		
		this.addTodo=this.addTodo.bind(this);
		this.deleteTodo=this.deleteTodo.bind(this);
		this.toggleTodo=this.toggleTodo.bind(this);
		this.showTodo=this.showTodo.bind(this);
	}	

	addTodo(){ 
		  
		let newTodoElem = this.refs.txtTodoName.value;
		
		this.props.addTodo({
			todoElem:newTodoElem,
			todoId:uuidv1(),  
			toggled:false
		});  
		
		this.refs.txtTodoName.value="";	
	}
		
	deleteTodo(todoId){ 
		this.props.deleteTodo(todoId);
	}
	
	toggleTodo(todoId){
		this.props.toggleTodo(todoId);	
	}
	
	showTodo(){
		const todos=this.props.todos;
		return(
			<ul>
			{
				todos.map((todo,index) => {
					return (
						<li key={index}>
							<p  style={{textDecoration: todo.toggled? 'line-through':'none' }}>{todo.todoElem}</p>
								 
							<button onClick={ ()=>this.toggleTodo(todo.todoId)}> toggle </button>  			{/*encountering errors when ()=> is removed,what is the possible cause of it?*/}
							<button onClick={ ()=> this.deleteTodo(todo.todoId)}>  remove</button>
						</li>
					)
				})
			}
			</ul>
		)
	}
	
	render() { 
		return (
			<div className="App">
				<input placeholder="I have to ..."  ref="txtTodoName"    />
				<button type="button" onClick={ this.addTodo }>Add Todo</button>
				<div>
				{ this.showTodo() } 
				 </div>
			   					  
			</div>
		);
    }
}  

function mapStateToProps(state){
	return {
		todos:state
	}
}
 
export default  connect(mapStateToProps,{ addTodo ,deleteTodo,toggleTodo})(Home);
